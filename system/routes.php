<?php

$router->get('/', '\App\Controllers\HomeController@index');

// get_started config
$router->get('/setup', '\App\Controllers\SetupController@index');

// view
$router->get('/view', '\App\Controllers\HomeController@view');

// webhook
$router->get('/webhook', '\App\Controllers\WebhookController@get');
$router->post('/webhook', '\App\Controllers\WebhookController@post');

// test good reads
// $router->get('/gr', '\App\Controllers\HomeController@greard');
// $router->get('/scrap', '\App\Controllers\HomeController@scrap');
