<?php

namespace App\Controllers;

use App\Services\Facebook\Facebook;
use App\Services\GoodReads\GoodReads;
use App\Services\Watson\Watson;
use App\System\Request;

class WebhookController extends BaseController
{
    protected $verify_token;

    protected $facebookService;

    public function __construct()
    {
        $this->facebookService = new Facebook();

        parent::__construct();
    }

    public function get(Request $request)
    {
        $this->verify_token = isset($request->web_hook_verify_token) ? $request->web_hook_verify_token : null;
        if ($this->verifyFacebookToken($request->parameter('hub_verify_token'))) {
            echo $request->parameter('hub_challenge');
            exit;
        }
        return $this->respondForbidden();
    }

    public function post(Request $request)
    {
        $body = $request->getBody();

        if ($body->object === 'page') {
            foreach ($body->entry as $entry) {
                $event = $entry->messaging[0];
                $user_id = $event->sender->id;
                $userData = $this->readUserInfo($user_id);

                if (isset($event->message)) {
                    $message = $event->message;

                    if (isset($userData['search']) && $userData['search'] === 'id') {
                        // search by id
                    } else {
                        return $this->searchAndDisplayResults($user_id, $message->text);
                    }

                } else {
                    $this->log->info(json_encode($event));
                    if (isset($event->postback)) {
                        if ($event->postback->payload === 'getStartedAction') {
                            return $this->greetTheNewUser($user_id);

                        } else if ($event->postback->payload === 'searchByBookName') {
                            $this->log->info('searchByBookName');
                            $this->facebookService->sendMessageToUser($user_id, 'Whats the books name?');
                            $this->writeUserInfo($user_id, ['search' => 'name']);
                            return $this->respond('OK');

                        } else if ($event->postback->payload === 'searchByBookId') {
                            $this->log->info('searchByBookId');
                            $this->facebookService->sendMessageToUser($user_id, 'Whats the books id?');
                            $this->writeUserInfo($user_id, ['search' => 'id']);
                            return $this->respond('OK');

                        } else {
                            if (strpos($event->postback->payload, 'getBookReview-') === 0) {

                                // see if evaluation on going
                                if (isset($userData['evaluating']) && $userData['evaluating'] === $event->postback->payload) {
                                    $this->log->info('duplicate fellow');
                                    return $this->respond('OK');

                                    // start evaluation
                                } else {
                                    $userData['evaluating'] = $event->postback->payload;
                                    $this->writeUserInfo($user_id, $userData);

                                    $this->facebookService->sendMessageToUser($user_id, 'Evaluating your choice, this may take sometime..');

                                    $book_id = str_replace('getBookReview-', '', $event->postback->payload);
                                    $book = $this->getBook($book_id);

                                    if ($book !== null) {
                                        $this->facebookService->sendMessageToUser($user_id, 'Interesting choice ' . $book['book']['title']);
                                        $reviews = $this->getBookReviews($book['book']['isbn']);

                                        if ($reviews !== null) {
                                            $this->facebookService->sendMessageToUser($user_id, 'Please hold on a bit longer as I analyze the data');
                                            $watson = new Watson();
                                            $isbn = base64_encode($book['book']['isbn']);
                                            $analyze = $watson->semanticAnalyze($isbn);

                                            if ($analyze) {
                                                $this->log->info($analyze->sentiment->document->label);
                                                if ($analyze->sentiment->document->label === 'positive') {
                                                    $this->facebookService->sendMessageToUser($user_id, 'I would recommend you buy this book, it seems an epic read. Enjoy!');

                                                } else if ($analyze->sentiment->document->label === 'neutral') {
                                                    $this->facebookService->sendMessageToUser($user_id, 'Hmm.. I cant seem to be able to make up my mind.');

                                                } else {
                                                    $this->facebookService->sendMessageToUser($user_id, 'Seems like this book aint that great, wanna try another book?');
                                                }
                                            } else {
                                                $this->facebookService->sendMessageToUser($user_id, 'I\'m sorry, but I am unable to analyze that data');
                                            }

                                        } else {
                                            $this->facebookService->sendMessageToUser($user_id, 'Hmm.. no one has reviewed this book. Would you read it and let us know how it is?');
                                        }

                                    } else {
                                        $this->facebookService->sendMessageToUser($user_id, 'Oh No! I cant find that book!');
                                    }
                                }

                                $this->log->info($book_id);
                                return $this->respond('OK');
                            }
                        }
                    }
                }
            }
        } else {
            return $this->respondNotFound();
        }
    }

    protected function greetTheNewUser($user_id)
    {
        $user = $this->facebookService->getUserFirstName($user_id);

        if ($user !== null) {
            $greet = $this->facebookService->greetTheUser($user_id, $user->first_name);
            $button_template = $this->facebookService->sendButtonTemplate($user_id);
        }

        $this->log->info(
            json_encode([
                'request' => $_REQUEST,
                'user' => $user,
                'greet' => isset($greet) ? $greet : 'null',
                'button_template' => isset($button_template) ? $button_template : 'null',
            ])
        );

        return $this->respond('OK');
    }

    protected function verifyFacebookToken($hub_verify_token)
    {
        return $hub_verify_token !== null && $hub_verify_token === $this->verify_token;
    }

    protected function searchAndDisplayResults($user_id, $keyword)
    {
        $gr = new GoodReads();
        $search = $gr->search($keyword);

        if ((int) $search['search']['total-results'] !== 0) {
            $resultsAll = $search['search']['results']['work'];
            $results = array_slice($resultsAll, 0, 4);

            $elements = [];
            foreach ($results as $result) {
                $elements[] = $this->facebookService->addListTemplateElement($result);
            }

            $this->facebookService->sendMessageToUser($user_id, 'Here are top results for  ' . $keyword . ' from Goodreads');
            $list = $this->facebookService->sendListTemplate($user_id, $elements);

            $this->log->info(
                json_encode([
                    $list,
                    count($elements),
                ])
            );

            return $this->respond('OK');

        } else {
            $this->facebookService->sendMessageToUser($user_id, 'Sorry, I couldn\'t find any thing for  ' . $keyword);
            return $this->respond('OK');
        }
    }

    public function getBook($book_id)
    {
        $gr = new GoodReads();
        $book = $gr->getBook($book_id);

        $this->log->info('Got the book info..');

        return $book;
    }

    public function getBookReviews($isbn)
    {
        $gr = new GoodReads();
        $reviews = $gr->getBookReviews($isbn);

        $this->storeData(base64_encode($isbn), json_encode($reviews));

        $this->log->info('Got the book reviews..');

        return $reviews;
    }

    protected function writeUserInfo($uid, $data)
    {
        $this->storeData(base64_encode('user-' . $uid), json_encode($data));
    }

    protected function readUserInfo($uid)
    {
        return $this->readData(base64_encode('user-' . $uid), false);
    }
}
