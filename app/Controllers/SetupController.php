<?php

namespace App\Controllers;

use App\Services\Facebook\Facebook;

class SetupController extends BaseController
{
    public function index()
    {
        $fb = new Facebook();
        $setupGetStarted = $fb->setupGetStarted();
        if (isset($setupGetStarted->result) && $setupGetStarted->result === 'success') {
            echo "<p>get_started setup successfull</p>";
        } else {
            echo "<p>get_started setup FAILED</p>";
        }
        exit;
    }
}
