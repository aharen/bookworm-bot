<?php

namespace App\System;

class Request
{
    protected $request;

    protected $parameters;

    public function __construct()
    {
        foreach ($_SERVER as $key => $value) {
            $this->{strtolower($key)} = $value;
        }

        $this->buildParameters();
    }

    protected function buildParameters()
    {
        if ($this->request_method === 'GET') {
            foreach ($_GET as $key => $value) {
                $this->parameters[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }

        if ($this->request_method === 'POST') {
            foreach ($_GET as $key => $value) {
                $this->parameters[$key] = filter_input(INPUT_GET, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }

            foreach ($_POST as $key => $value) {
                $this->parameters[$key] = filter_input(INPUT_POST, $key, FILTER_SANITIZE_SPECIAL_CHARS);
            }
        }
    }

    public function parameters()
    {
        return $this->parameters;
    }

    public function parameter($key = null)
    {
        return (is_null($key)) ? $this->parameters : isset($this->parameters[$key]) ? $this->parameters[$key] : null;
    }

    public function getBody()
    {
        return json_decode(file_get_contents("php://input"));
    }
}
