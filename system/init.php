<?php

// load the helpers
require 'helpers.php';

// initiate the request, the router
$router = new App\System\Router(
    new App\System\Request()
);

// require the routes
require 'routes.php';

die();
