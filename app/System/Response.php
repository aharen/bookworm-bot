<?php

namespace App\System;

class Response
{
    protected $content;

    protected $headers;

    protected $statusCode;

    public function __construct($content, $statusCode = 200)
    {
        $this->content = $content;
        $this->setStatusCode($statusCode);
    }

    public function toJson()
    {
        header('Content-Type: application/json');
        return json_encode($this->content);
    }

    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
        http_response_code($this->statusCode);
    }
}
