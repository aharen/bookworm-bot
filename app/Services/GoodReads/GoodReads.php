<?php

namespace App\Services\GoodReads;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\HandlerStack;
use GuzzleXml\XmlMiddleware;
use Symfony\Component\DomCrawler\Crawler;

class GoodReads
{
    protected $host = 'https://www.goodreads.com/';

    protected $api_key;

    protected $api_secret;

    protected $client;

    public function __construct()
    {
        $this->api_key = getenv('GR_KEY');
        $this->api_secret = getenv('GR_SECRET');

        $stack = HandlerStack::create();
        $stack->push(XmlMiddleware::xml(), 'xml');
        $this->client = new Client([
            'handler' => $stack,
            'base_uri' => $this->host,
            'headers' => [
                'Accept' => 'application/xml',
            ],
        ]);
    }

    public function search($keyword, $search = 'title')
    {
        $method = http_build_query([
            'key' => $this->api_key,
            'q' => urlencode($keyword),
            'search' => $search,
        ]);
        return $this->get('search.xml?' . $method);
    }

    public function getBook($book_id)
    {
        $method = http_build_query([
            'key' => $this->api_key,
            'id' => $book_id,
            'page' => 2,
        ]);
        return $this->get('book/show?' . $method);
    }

    public function getBookReviews($isbn)
    {
        $host = 'https://www.goodreads.com/api/reviews_widget_iframe?';
        $params = [
            'hide_last_page' => true,
            'isbn' => $isbn,
            'num_reviews' => 50,
            'page' => 1,
        ];

        $reviews = [];
        $loop = true;

        for ($i = 1; $loop === true; $i++) {

            $html = $this->getHtmlContent($i, $host, $params);

            $crawler = new Crawler($html);
            $content = $crawler->filter('.gr_reviews_container > .gr_review_container');

            if ($content->count() > 0) {
                $reviewsTmp = $content->each(function (Crawler $node, $i) {
                    $rating = trim($node->filter('.gr_rating')->text());
                    $dt = Carbon::parse(trim($node->filter('.gr_review_date')->text()));

                    return [
                        'by' => trim($node->filter('.gr_review_by')->text()),
                        'rating' => $rating,
                        'rating_number' => $this->getRatingNumber($rating),
                        'date' => $dt->format('Y-m-d'),
                        'text' => trim($node->filter('.gr_review_text')->text()),
                    ];
                });

                $reviews = array_merge($reviews, $reviewsTmp);

                continue;
            }
            $loop = false;
        }

        return $reviews;
    }

    protected function getHtmlContent($i, $host, $params)
    {
        $params['page'] = $i;
        $host .= http_build_query($params);

        $client = new Client();
        $res = $client->request('GET', $host);
        return $res->getBody()->getContents();
    }

    protected function getRatingNumber($rating)
    {
        switch ($rating) {
            case '★★★★★':
                return 5;
                break;

            case '★★★★☆':
                return 4;
                break;

            case '★★★☆☆':
                return 3;
                break;

            case '★★☆☆☆':
                return 2;
                break;

            case '★☆☆☆☆':
                return 1;
                break;

            default:
                return 0;
                break;
        }
    }

    protected function get($method)
    {
        try {
            $response = $this->client->get($method);
            $body = (string) $response->getBody();
            return $this->handleXMLResponse(
                $body
            );
        } catch (RequestException $e) {
            $data = $e->getMessage();
        } catch (ClientException $e) {
            $data = $e->getMessage();
        }
        return $data;
    }

    protected function handleXMLResponse($response)
    {
        return json_decode(json_encode((array) simplexml_load_string($response, 'SimpleXMLElement', LIBXML_NOCDATA)), 1);
    }
}
