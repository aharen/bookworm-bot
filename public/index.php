<?php

// require the autoloader
require __DIR__ . '/../vendor/autoload.php';

// load environment variables
$dotenv = new Dotenv\Dotenv(__DIR__ . '/../');
$dotenv->load();

// initiate
require __DIR__ . '/../system/init.php';
