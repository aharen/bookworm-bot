<?php

namespace App\System;

use App\System\Request;

class Router
{
    protected $request;

    protected $request_methods = [
        'GET',
        'POST',
    ];

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function __call($name, $args)
    {
        list($route, $method) = $args;
        if (!in_array(strtoupper($name), $this->request_methods)) {
            $this->invalidMethodHandler();
        }
        $this->{strtolower($name)}[$this->formatRoute($route)] = $method;
    }

    protected function formatRoute($route)
    {
        $routeOnly = explode('?', $route);
        $result = rtrim($routeOnly[0], '/');
        return ($result === '') ? '/' : $result;
    }

    protected function invalidMethodHandler()
    {
        header("{$this->request->server_protocol} 405 Method Not Allowed");
        echo json_encode(['error' => 'Method Not Allowed']);
        exit;
    }

    protected function notFoundHandler()
    {
        header("{$this->request->server_protocol} 404 Not Found");
        echo json_encode(['error' => '404 Not Found']);
        exit;
    }

    protected function resolveClass($method)
    {
        list($className, $functionName) = explode('@', $method);
        $class = new $className();
        if (method_exists($class, $functionName)) {
            echo call_user_func_array([$class, $functionName], [$this->request]);
        } else {
            return $this->notFoundHandler();
        }
    }

    protected function resolveFunction($method)
    {
        echo call_user_func_array($method, [$this->request]);
    }

    public function resolve()
    {
        $routerMethods = $this->{strtolower($this->request->request_method)};
        $formatedRoute = $this->formatRoute($this->request->request_uri);
        $method = isset($routerMethods[$formatedRoute]) ? $routerMethods[$formatedRoute] : null;

        if (is_null($method)) {
            $this->notFoundHandler();
        }

        return (is_string($method)) ? $this->resolveClass($method) : $this->resolveFunction($method);
    }

    public function __destruct()
    {
        $this->resolve();
    }
}
