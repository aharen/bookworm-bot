<?php

namespace App\Services\Watson;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Watson
{
    protected $host = 'https://gateway.watsonplatform.net/natural-language-understanding/api/v1/analyze';

    protected $client;

    protected $api_key;

    protected $app_uri;

    public function __construct()
    {
        $this->api_key = getenv('IBM_API_KEY');
        $this->app_uri = getenv('APP_URI');

        $this->client = new Client([
            'base_uri' => $this->host,
            'headers' => [
                'Content-Type' => 'application/json',
                'Content-Type' => 'charset=utf-8',
                'Accept' => 'application/json',
                'Cache-Control' => 'no-cache',
                'Authorization' => 'Basic ' . base64_encode('apikey:' . $this->api_key),
            ],
        ]);
    }

    public function semanticAnalyze($id)
    {
        $uri = $this->app_uri . '/view?id=' . $id;

        return $this->get([
            'version' => '2018-03-19',
            'url' => $uri,
            'features' => 'sentiment,semantic_roles',
            'return_analyzed_text' => true,
        ]);
    }

    protected function get($data)
    {
        try {
            $response = $this->client->get('analyze', ['query' => $data]);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
        } catch (RequestException $e) {
            $data = $e->getMessage();
        } catch (ClientException $e) {
            $data = $e->getMessage();
        }
        return $data;
    }
}
