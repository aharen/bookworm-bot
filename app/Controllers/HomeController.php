<?php

namespace App\Controllers;

use App\Services\GoodReads\GoodReads;
use App\Services\Watson\Watson;
use App\System\Request;

class HomeController extends BaseController
{
    public function index()
    {
        return $this->respond('Hi! o.O');
    }

    public function view(Request $request)
    {
        $id = $request->parameter('id');
        if ($id !== null) {
            $data = $this->readData($id);
            return $this->respond(
                $data
            );
        }
        return $this->respondNotFound();
    }

    public function greard()
    {
        $watson = new Watson();

        $id = base64_encode('0439554934');
        return $this->respond(
            $watson->semanticAnalyze($id)
        );
    }

    public function scrap()
    {
        $gr = new GoodReads();
        $reviews = $gr->getBookReviews('0439554934');

        $this->storeData(base64_encode('0439554934'), json_encode($reviews));

        return $this->respond(
            $reviews
        );

        // $isbn = '0439554934';
        // $host = 'https://www.goodreads.com/api/reviews_widget_iframe?';
        // $params = [
        //     'hide_last_page' => true,
        //     'isbn' => $isbn,
        //     'num_reviews' => 50,
        //     'page' => 1,
        // ];

        // $reviews = [];
        // $loop = true;

        // for ($i = 1; $loop === true; $i++) {

        //     $params['page'] = $i;
        //     $host .= http_build_query($params);

        //     // dd($host);

        //     $client = new Client();
        //     $res = $client->request('GET', $host);
        //     $html = $res->getBody()->getContents();

        //     // echo $html;
        //     // $reviews = [];

        //     $crawler = new Crawler($html);
        //     $content = $crawler->filter('.gr_reviews_container > .gr_review_container');

        //     if ($content->count() > 0) {
        //         $reviewsTmp = $content->each(function (Crawler $node, $i) {
        //             $rating = trim($node->filter('.gr_rating')->text());

        //             return [
        //                 'by' => trim($node->filter('.gr_review_by')->text()),
        //                 'rating' => $rating,
        //                 'rating_number' => $this->getRatingNumber($rating),
        //                 'date' => trim($node->filter('.gr_review_date')->text()),
        //                 'text' => trim($node->filter('.gr_review_text')->text()),
        //             ];
        //         });
        //         $reviews = array_merge($reviews, $reviewsTmp);

        //         /*
        //         FOR TESTIST
        //          */

        //         // if ($i === 3) {
        //         //     $loop = false;
        //         // }

        //         continue;

        //     }

        //     $loop = false;
        // }

        // var_dump($reviews);

        // // $crawler = $crawler->filter('body > div.gr_reviews_container');

        // // foreach ($crawler as $domElement) {
        // //     var_dump(
        // //         $domElement
        // //     );
        // // }

        // // dd(
        // //     $crawler->toString()
        // // );
    }

    public function getRatingNumber($rating)
    {
        switch ($rating) {
            case '★★★★★':
                return 5;
                break;

            case '★★★★☆':
                return 4;
                break;

            case '★★★☆☆':
                return 3;
                break;

            case '★★☆☆☆':
                return 2;
                break;

            case '★☆☆☆☆':
                return 1;
                break;

            default:
                return 0;
                break;
        }
    }
}
