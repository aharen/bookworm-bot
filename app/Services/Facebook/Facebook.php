<?php

namespace App\Services\Facebook;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\RequestException;

class Facebook
{
    protected $host = 'https://graph.facebook.com/';

    protected $client;

    protected $token;

    public function __construct()
    {
        $this->token = getenv('FB_TOKEN');
        $this->client = new Client([
            'base_uri' => $this->host,
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' . $this->token,
            ],
        ]);
    }

    public function getUserFirstName($user_id)
    {
        $uri = 'v3.2/' . $user_id . '?fields=first_name';
        return $this->get($uri);
    }

    public function sendMessageToUser($user_id, $message)
    {
        $uri = 'v2.6/me/messages';
        $data = [
            'form_params' => [
                'messaging_type' => 'RESPONSE',
                'recipient' => [
                    'id' => $user_id,
                ],
                'message' => [
                    'text' => $message,
                ],
                "postback",
            ],
        ];
        return $this->post($uri, $data);
    }

    public function greetTheUser($user_id, $first_name)
    {
        return $this->sendMessageToUser(
            $user_id,
            "Hello $first_name! \n\nI am Bookworm Bot! I can help you evaluate awesome books!"
        );
    }

    public function setupGetStarted()
    {
        $uri = 'v2.6/me/messenger_profile';
        $data = [
            'form_params' => [
                "get_started" => [
                    'payload' => 'getStartedAction',
                ],
            ],
        ];
        return $this->post($uri, $data);
    }

    public function sendListTemplate($user_id, $elements)
    {
        $payload = [
            'template_type' => 'list',
            "top_element_style" => "compact",
            'elements' => $elements,
        ];

        return $this->sendTemplate($user_id, $payload);
    }

    public function addListTemplateElement($element)
    {
        return [
            "title" => $element['best_book']['title'],
            "subtitle" => 'by ' . $element['best_book']['author']['name'],
            "image_url" => $element['best_book']['image_url'],
            "buttons" => [
                [
                    'type' => 'postback',
                    'title' => 'Evaluate',
                    'payload' => 'getBookReview-' . $element['best_book']['id'],
                ],
            ],
        ];
    }

    public function sendButtonTemplate($user_id)
    {
        $payload = [
            'template_type' => 'button',
            'text' => 'How would you like to search for a book?',
            'buttons' => [
                [
                    'type' => 'postback',
                    'title' => 'Book Name',
                    'payload' => 'searchByBookName',
                ], [
                    'type' => 'postback',
                    'title' => 'GoodReads Id',
                    'payload' => 'searchByBookId',
                ],
            ],
        ];

        return $this->sendTemplate($user_id, $payload);
    }

    public function sendTemplate($user_id, $payload)
    {
        $uri = 'v2.6/me/messages';
        $data = [
            'form_params' => [
                'recipient' => [
                    'id' => $user_id,
                ],
                'message' => [
                    'attachment' => [
                        'type' => 'template',
                        'payload' => $payload,
                    ],
                ],
            ],
        ];
        return $this->post($uri, $data);
    }

    protected function post($method, $data)
    {
        try {
            $response = $this->client->post($method, $data);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
        } catch (RequestException $e) {
            $data = $e->getMessage();
        } catch (ClientException $e) {
            $data = $e->getMessage();
        }
        return $data;
    }

    protected function get($method)
    {
        try {
            $response = $this->client->get($method);
            $body = $response->getBody();
            $data = json_decode($body->getContents());
        } catch (RequestException $e) {
            $data = $e->getMessage();
        } catch (ClientException $e) {
            $data = $e->getMessage();
        }
        return $data;
    }
}
