<?php

namespace App\Controllers;

use App\System\Response;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\Streamhandler;
use Monolog\Logger;

class BaseController
{
    protected $log;

    protected $data_dir;

    public function __construct()
    {
        $this->setLogger();
        $this->data_dir = __DIR__ . '/../../data/';
    }

    public function respond($data, $statusCode = 200)
    {
        $response = new Response($data, $statusCode);
        return $response->toJson();
    }

    public function respondNotFound()
    {
        return $this->respond('Not Found', 404);
    }

    public function respondForbidden()
    {
        return $this->respond('Forbidden', 403);
    }

    protected function setLogger()
    {
        $logFile = __DIR__ . '/../../logs/logger.log';

        $this->log = new Logger('bookworm-bot');
        $stream = new Streamhandler($logFile, Logger::DEBUG);
        $stream->setFormatter(new JsonFormatter());
        $this->log->pushHandler($stream);
    }

    protected function storeData($filename, $data)
    {
        $file = $this->data_dir . $filename . '.json';

        $this->log->info($filename);

        if (!file_exists($file)) {
            touch($file);
        }

        $fh = fopen($file, 'w');
        if (fwrite($fh, $data) === false) {
            $this->log->error('Write to file failed!');
        }
        fclose($fh);
    }

    protected function readData($filename, $sort = true)
    {
        $file = $this->data_dir . $filename . '.json';

        if (!file_exists($file)) {
            return $this->respondNotFound();
        }

        $data = json_decode(file_get_contents($file), true);

        if ($sort) {
            $data = array_slice($data, 0, 50);
            return $this->multiDimentionalArraySortByValue($data, 'date');
        } else {
            return $data;
        }
    }

    protected function multiDimentionalArraySortByValue($data, $key)
    {
        $dates = array_column($data, $key);
        array_multisort($dates, SORT_DESC, $data);
        return $data;
    }
}
